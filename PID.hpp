/** PID.hpp
 *
 * @version 1.0.0
 * @author David Martín <david.martin.zaragoza@gmail.com>
 * @license GNU General Public License v3.0
 *
 */

#ifndef _PID_HPP
#define _PID_HPP

#include "reespirator/Core/Types/integers.h"

// don't touch: this define is passed by environment
#ifndef FIRMWARE_VERSION_MAJOR
#define FIRMWARE_VERSION_MAJOR 0
#endif 

// don't touch: this define is passed by environment
#ifndef FIRMWARE_VERSION_MINOR
#define FIRMWARE_VERSION_MINOR 0
#endif 

namespace reespirator 
{

class PID
{

    public:

        /**
         * @brief Construct a new PID object.
         * kP, kI, kD: Integers coded with 2 decimal positions.
         */
        PID(u8 kP, u8 kI, u8 kD, u8 maxSlopeP, u8 maxSlopeD, s16 outputMin, s16 outputMax, u8 timeStep);

        /**
         * @brief Allows setting the PID parameters.
         * kP, kI, kD: Integers coded with 2 decimal positions.
         */
        void setParameters(u8 kP, u8 kI, u8 kD, u8 maxSlopeP, u8 maxSlopeD, s16 outputMin, s16 outputMax);

        /**
         * @brief Allows setting the Proportional constant.
         */
        void setKP(u8 kP);

        /**
         * @brief Allows setting the Integral constant.
         */
        void setKI(u8 kI);

        /**
         * @brief Allows setting the Derivative constant.
         */
        void setKD(u8 kD);

        /**
         * @brief Allows setting the Maximum Proportional Slope.
         */
        void setMaxSlopeP(u8 maxSlopeP);

        /**
         * @brief Allows setting the Maximum Derivative Slope.
         */
        void setMaxSlopeD(u8 maxSlopeD);

        /**
         * @brief Checks if the sistem feedback is in within the given distance to the setpoint.
         */
        bool atSetpoint(u8 positiveThreshold, u8 negativeThreshold);

        /**
         * @brief Calculates PID and returns Actuation (Output) value.
         */
        u16 calculateActuation(s16 setpoint, s16 feedback);

        /**
         * @brief Resets the PID.
         */
        void reset();

    private:

        u8 _kP, _kI, _kD; //Integers coded with 2 decimal positions.
        u8 _maxSlopeP, _maxSlopeD;
        s16 _outputMin, _outputMax;
        u8 _timeStep; // milliseconds
        s8 _error;
        s8 _previousError;
        s8 _errorIntegral;

};

}; // namespace reespirator  

#endif //_PID_HPP