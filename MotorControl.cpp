/** MotorControl.cpp
 *
 * MotorControl class.
 *
 * @version 1.0.0
 * @author David Martín <david.martin.zaragoza@gmail.com>
 * @license GNU General Public License v3.0
 *
 */


#include <reespirator/Core/DRE.hpp>
#include "PID.hpp"
#include "MotorControl.hpp"

namespace reespirator {

DRE _dre;
s16 tempPeakPressure; // Temporary Peak Pressure (cmH2O). Signed integer coded with 2 decimal position.
s16 tempPeepPressure; // Temporary Peep Pressure (cmH2O). Signed integer coded with 2 decimal position.
s16 _pressureSetpoint; //TODO: move to dre to debug_plot
s16 _p0; // Initial pressure at cycle start (insufflation start)
s16 _positionSetpoint; //TODO: move to dre to debug_plot
s16 _pos0; // Initial possition at exsufflation start
s16 _flowSetpoint; //TODO: move to dre to debug_plot
u16 _constantFlowTime;
u8 _initialRecruitTimerValue;
u16 _exsPauseTime;
u16 _exsRampTime;
bool readyToTrigger;

//TODO: Some of these vars can be #defined
//TODO: May be group in struct
//Integers coded with 2 decimal positions.
u8 mechVentPres_KP = (u8)(1.0 * 100);
u8 mechVentPres_KI = (u8)(0.0 * 100);
u8 mechVentPres_KD = (u8)(0.0 * 100);
u8 mechVentPres_MaxSlopeP = (u8)(100.0 * 100);
u8 mechVentPres_MaxSlopeD = (u8)(1.0 * 100);
s16 mechVentPres_OutputMin = (u16)(1.0 * 100);
s16 mechVentPres_OutputMax = (u16)(1.0 * 100);

u8 mechVentFlow_KP = (u8)(1.0 * 100);
u8 mechVentFlow_KI = (u8)(0.0 * 100);
u8 mechVentFlow_KD = (u8)(0.0 * 100);
u8 mechVentFlow_MaxSlopeP = (u8)(100.0 * 100);
u8 mechVentFlow_MaxSlopeD = (u8)(1.0 * 100);
s16 mechVentFlow_OutputMin = (u16)(1.0 * 100);
s16 mechVentFlow_OutputMax = (u16)(1.0 * 100);

u8 mechVentPos_KP = (u8)(1.0 * 100);
u8 mechVentPos_KI = (u8)(0.0 * 100);
u8 mechVentPos_KD = (u8)(0.0 * 100);
u8 mechVentPos_MaxSlopeP = (u8)(100.0 * 100);
u8 mechVentPos_MaxSlopeD = (u8)(1.0 * 100);
s16 mechVentPos_OutputMin = (u16)(1.0 * 100);
s16 mechVentPos_OutputMax = (u16)(1.0 * 100);

u8 peepValve_KP = (u8)(1.0 * 100);
u8 peepValve_KI = (u8)(0.0 * 100);
u8 peepValve_KD = (u8)(0.0 * 100);
u8 peepValve_MaxSlopeP = (u8)(100.0 * 100);
u8 peepValve_MaxSlopeD = (u8)(1.0 * 100);
s16 peepValve_OutputMin = 0;
s16 peepValve_OutputMax = PEEP_VALVE_CLOSED;

static PID mechVentPID(mechVentPos_KP, mechVentPos_KI, mechVentPos_KD, mechVentPos_MaxSlopeP, mechVentPos_MaxSlopeD, mechVentPos_OutputMin, mechVentPos_OutputMax, 5 /* _dre.getTimeStep */);
static PID peepValvePID(peepValve_KP, peepValve_KI, peepValve_KD, peepValve_MaxSlopeP, peepValve_MaxSlopeD, peepValve_OutputMin, peepValve_OutputMax, 5 /* _dre.getTimeStep */);

MotorControl::MotorControl(DRE& dre) :
    _dre(dre)
{
    
}

void MotorControl::enterInsPC(u32 now_millis)
{
    //Trigger management TODO: maybe move to be managed by state machine
    if (_dre.isTriggerDetected()) {
        _dre.setCycleTriggered(true);
        _dre.setTriggerDetected(false);
    } else {
        _dre.setCycleTriggered(false);
    }

    //Calculate cycle times
    _dre.setCycleTime(((60 * 100) / _dre.getRpmParam())*10); // Respiratoty cycle time [msec]
    _dre.setInsTime((_dre.getCycleTime() / (10 + _dre.getIeRatioDivisorParam())) * 10);
    _dre.setExsTime(_dre.getCycleTime() - _dre.getInsTime());

    _dre.setInsRampTime( ((u32)_dre.getInsTime() * (_dre.getRampParam() + 1)) / 256 );

    _dre.setLastCycleStartTime(_dre.getCycleStartTime());
    _dre.setCycleStartTime(now_millis);

    tempPeakPressure = _dre.getPressureMeasure(); //Reset tempPeakPressure
    _p0 = _dre.getPressureMeasure();
    _dre.setVolumeMeasure(0); //Reset Volume Measure (flow integral)

    _dre.setPeepValveActuation(PEEP_VALVE_CLOSED);
    
    mechVentPID.reset();
    mechVentPID.setParameters(mechVentPres_KP, mechVentPres_KI, mechVentPres_KD, mechVentPres_MaxSlopeP, mechVentPres_MaxSlopeD, mechVentPres_OutputMin, mechVentPres_OutputMax);
}

void MotorControl::updateInsPC(u32 now_millis)
{
    //TODO: Atomic (or temp var for _dre.getPressureMeasure)
    if (_dre.getPressureMeasure() > tempPeakPressure) {
        tempPeakPressure = _dre.getPressureMeasure();
    }

    //Update Volume Measure (flow integral)
    //_dre.setVolumeMeasure(_dre.getVolumeMeasure() + (((_dre.getFlowMeasure() + _dre.getPreviousFlowMeasure()) / 2) * _dre.getTimeStep()));
    _dre.setVolumeMeasure(_dre.getVolumeMeasure() + (_dre.getFlowMeasure() * _dre.getTimeStep()));

    //Calculate current setpoint (Pressure)
    u16 ellapsedMillis = (u16)now_millis - (u16)(_dre.getCycleStartTime()); //TODO: check behaviour on each overflow case
    if (ellapsedMillis < _dre.getInsRampTime()) {
        //Ramp setpoint calculation (_t0 is always 0 in the inspiration ramp)
        _pressureSetpoint = _p0 + (_dre.getPeakPressureParam() - _p0) / (_dre.getInsRampTime() /* - _t0 */) * ellapsedMillis;
    } else {
        _pressureSetpoint = _dre.getCyclePeakPressure();
    }

    //Calculate PID Actuation, as MechVent speed        // TODO: (steps/second)??
    _dre.setMechVentActuation_Speed(mechVentPID.calculateActuation(_pressureSetpoint, _dre.getPressureMeasure()));
}

void MotorControl::exitInsPC(u32 now_millis)
{
    _dre.setCyclePeakPressure( (tempPeakPressure > 0) ? (tempPeakPressure / 10) : 0);
    _dre.setCyclePlateauPressure( (_dre.getPressureMeasure() > 0) ? (_dre.getPressureMeasure() / 10) : 0 );
    _dre.setCycleVolume(_dre.getVolumeMeasure());

    //Generate flag (here or in state machine) to send Frame type 4 - Cycle data
}

void MotorControl::enterInsVC(u32 now_millis) //TODO: implement for advanced features product
{
    //Trigger management TODO: maybe move to be managed by state machine
    if (_dre.isTriggerDetected()) {
        _dre.setCycleTriggered(true);
        _dre.setTriggerDetected(false);
    } else {
        _dre.setCycleTriggered(false);
    }

    //Calculate cycle times
    _dre.setCycleTime(((60 * 100) / _dre.getRpmParam())*10); // Respiratoty cycle time [msec]
    _dre.setInsTime((_dre.getCycleTime() / (10 + _dre.getIeRatioDivisorParam())) * 10);
    _dre.setExsTime(_dre.getCycleTime() - _dre.getInsTime());

    u16 tempVal = _dre.getInsTime() >> 2; //_dre.getInsTime / 4 for calculating 25%
    _dre.setPauseTime( (tempVal <= 300) ? tempVal : 300 ); //limit to 300ms max
    _constantFlowTime = _dre.getInsTime() - _dre.getPauseTime();

    _dre.setLastCycleStartTime(_dre.getCycleStartTime());
    _dre.setCycleStartTime(now_millis);

    tempPeakPressure = _dre.getPressureMeasure(); //Reset tempPeakPressure
    _dre.setVolumeMeasure(0); //Reset Volume Measure (flow integral)

    _dre.setPeepValveActuation(PEEP_VALVE_CLOSED);
    
    //Calculate setpoint (Constant Flow)
    _flowSetpoint = _dre.getVolumeParam() / _constantFlowTime;

    mechVentPID.reset();
    mechVentPID.setParameters(mechVentFlow_KP, mechVentFlow_KI, mechVentFlow_KD, mechVentFlow_MaxSlopeP, mechVentFlow_MaxSlopeD, mechVentFlow_OutputMin, mechVentFlow_OutputMax);
}

void MotorControl::updateInsVC(u32 now_millis) //TODO: implement for advanced features product
{   
    //TODO: Atomic (or temp var for _dre.getPressureMeasure)
    if (_dre.getPressureMeasure() > tempPeakPressure) {
        tempPeakPressure = _dre.getPressureMeasure();
    }

    //Update Volume Measure (flow integral)
    //_dre.setVolumeMeasure(_dre.getVolumeMeasure() + (((_dre.getFlowMeasure() + _dre.getPreviousFlowMeasure()) / 2) * _dre.getTimeStep()));
    _dre.setVolumeMeasure(_dre.getVolumeMeasure() + (_dre.getFlowMeasure() * _dre.getTimeStep()));

    //Apply constant setpoint (Flow), or 0 when in pause time.
    u16 ellapsedMillis = (u16)now_millis - (u16)(_dre.getCycleStartTime()); //TODO: check behaviour on each overflow case
    if (ellapsedMillis > _constantFlowTime) {
        _flowSetpoint = 0;
    }

    //Calculate PID Actuation, as MechVent speed        // TODO: (steps/second)??
    _dre.setMechVentActuation_Speed(mechVentPID.calculateActuation(_flowSetpoint, _dre.getFlowMeasure()));
}

void MotorControl::exitInsVC(u32 now_millis) { //TODO: implement for advanced features product
    _dre.setCyclePeakPressure( (tempPeakPressure > 0) ? (tempPeakPressure / 10) : 0);
    _dre.setCyclePlateauPressure( (_dre.getPressureMeasure() > 0) ? (_dre.getPressureMeasure() / 10) : 0 );
    _dre.setCycleVolume(_dre.getVolumeMeasure());

    //Generate flag (here or in state machine) to send Frame type 4 - Cycle data
}

void MotorControl::enterInsVCRP(u32 now_millis) {} //TODO: implement for advanced features product
void MotorControl::updateInsVCRP(u32 now_millis) {} //TODO: implement for advanced features product
void MotorControl::exitInsVCRP(u32 now_millis) {} //TODO: implement for advanced features product

void MotorControl::enterInsRecruit(u32 now_millis)
{
    //Trigger management TODO: maybe move to be managed by state machine
    _dre.setTriggerDetected(false);
    _dre.setCycleTriggered(false);

    //Calculate cycle times
    _dre.setCycleTime(((60 * 100) / _dre.getRpmParam())*10); // Respiratoty cycle time [msec]
    _dre.setInsTime((_dre.getCycleTime() / (10 + _dre.getIeRatioDivisorParam())) * 10);
    _dre.setExsTime(_dre.getCycleTime() - _dre.getInsTime());

    _dre.setInsRampTime( ((u32)_dre.getInsTime() * (_dre.getRampParam() + 1)) / 256 );

    _dre.setLastCycleStartTime(_dre.getCycleStartTime());
    _dre.setCycleStartTime(now_millis);
    _initialRecruitTimerValue = _dre.getRecruitTimerParam();

    tempPeakPressure = _dre.getPressureMeasure(); //Reset tempPeakPressure
    _p0 = _dre.getPressureMeasure();
    _dre.setVolumeMeasure(0); //Reset Volume Measure (flow integral)

    _dre.setPeepValveActuation(PEEP_VALVE_CLOSED);
    
    mechVentPID.reset();
    mechVentPID.setParameters(mechVentPres_KP, mechVentPres_KI, mechVentPres_KD, mechVentPres_MaxSlopeP, mechVentPres_MaxSlopeD, mechVentPres_OutputMin, mechVentPres_OutputMax);
}

void MotorControl::updateInsRecruit(u32 now_millis)
{
    u16 ellapsedMillis = (u16)now_millis - (u16)(_dre.getCycleStartTime()); //TODO: check behaviour on each overflow case

    //Update recruitTimerParam by discounting seconds from _initialRecruitTimerValue.
    u8 tempRecruitTimer = _initialRecruitTimerValue - (ellapsedMillis/1000);
    if (_dre.getRecruitTimerParam() != tempRecruitTimer) //True once every second. 
    {
        //Update RecruitTimeParam
        _dre.setRecruitTimerParam(tempRecruitTimer);

        //Update Cycle Data
        _dre.setCyclePeakPressure( (tempPeakPressure > 0) ? (tempPeakPressure / 10) : 0);
        _dre.setCyclePlateauPressure( (_dre.getPressureMeasure() > 0) ? (_dre.getPressureMeasure() / 10) : 0 );
        _dre.setCycleVolume(_dre.getVolumeMeasure());
    }

    //TODO: _dre.getRecruitTimerParam(); must be monitored by state machine to end this Recruit Inspiration when = 0.

    //TODO: Atomic (or temp var for _dre.getPressureMeasure)
    if (_dre.getPressureMeasure() > tempPeakPressure) {
        tempPeakPressure = _dre.getPressureMeasure();
    }

    //Update Volume Measure (flow integral)
    //_dre.setVolumeMeasure(_dre.getVolumeMeasure() + (((_dre.getFlowMeasure() + _dre.getPreviousFlowMeasure()) / 2) * _dre.getTimeStep()));
    _dre.setVolumeMeasure(_dre.getVolumeMeasure() + (_dre.getFlowMeasure() * _dre.getTimeStep()));

    //Calculate current setpoint (Pressure)
    if (ellapsedMillis < _dre.getInsRampTime()) {
        //Ramp setpoint calculation (_t0 is always 0 in the inspiration ramp)
        _pressureSetpoint = _p0 + (_dre.getPeakPressureParam() - _p0) / (_dre.getInsRampTime() /* - _t0 */) * ellapsedMillis;
    } else {
        _pressureSetpoint = _dre.getCyclePeakPressure();
    }

    //Calculate PID Actuation, as MechVent speed        // TODO: (steps/second)??
    _dre.setMechVentActuation_Speed(mechVentPID.calculateActuation(_pressureSetpoint, _dre.getPressureMeasure()));
}

void MotorControl::exitInsRecruit(u32 now_millis)
{
    _dre.setCyclePeakPressure( (tempPeakPressure > 0) ? (tempPeakPressure / 10) : 0);
    _dre.setCyclePlateauPressure( (_dre.getPressureMeasure() > 0) ? (_dre.getPressureMeasure() / 10) : 0 );
    _dre.setCycleVolume(_dre.getVolumeMeasure());

    //Generate flag (here or in state machine) to send Frame type 4 - Cycle data
}

void MotorControl::enterExs(u32 now_millis)
{
    //Calculate cycle times
    //TODO: use #define, but first test and approve aproach and timing
    _exsPauseTime = 50; //Time before position ramp-down, to allow solenoid PEEP valve to open.
    _exsRampTime = _dre.getExsTime() * 0.3; //Exsufflation position ramp-down duration.
    if (_exsRampTime < 350) { _exsRampTime = 350; }
    if (_exsRampTime > 700) { _exsRampTime = 700; }

    _dre.setLastCycleStartTime(_dre.getCycleStartTime());
    _dre.setExsufflationStartTime(now_millis);

    tempPeepPressure = _dre.getPressureMeasure(); //Reset tempPeepPressure
    _pos0 = _dre.getMechVentActuation_Position();
    
    peepValvePID.reset();

    mechVentPID.reset();
    mechVentPID.setParameters(mechVentPos_KP, mechVentPos_KI, mechVentPos_KD, mechVentPos_MaxSlopeP, mechVentPos_MaxSlopeD, mechVentPos_OutputMin, mechVentPos_OutputMax);

    readyToTrigger = false;
}

void MotorControl::updateExs(u32 now_millis)
{
    //TODO: Atomic (or temp var for _dre.getPressureMeasure)
    if (_dre.getPressureMeasure() < tempPeepPressure) {
        tempPeepPressure = _dre.getPressureMeasure();
    }

    //Update Volume Measure (flow integral)
    _dre.setVolumeMeasure(_dre.getVolumeMeasure() + (((_dre.getFlowMeasure() + _dre.getPreviousFlowMeasure()) / 2) * _dre.getTimeStep()));

    //Calculate PID Actuation, as PeepValve closing force   // TODO: (units)??
    _dre.setPeepValveActuation(peepValvePID.calculateActuation(_dre.getPeepPressureParam(), _dre.getPressureMeasure()));

    if (!readyToTrigger) {
 
        //Calculate current setpoint (Position)
        u16 ellapsedMillis = (u16)now_millis - (u16)(_dre.getCycleStartTime()); //TODO: check behaviour on each overflow case
        if (ellapsedMillis < _exsPauseTime) {
            _positionSetpoint = _pos0;
        } else if (ellapsedMillis < (_exsPauseTime + _exsRampTime)) {
            //Ramp setpoint calculation
            _positionSetpoint = _pos0 / _exsRampTime * (ellapsedMillis - _exsPauseTime);
        } else {
            _positionSetpoint = 0;
            if (mechVentPID.atSetpoint(0, 0)) {
                readyToTrigger = true;
            }
        }
 
    } else {
        if ((_dre.getTriggerFlowParam() != 0xFFFF) && ((-_dre.getFlowMeasure()) >= (s16)(_dre.getTriggerFlowParam()) )) {
            _dre.setTriggerDetected(true);
        } 
    }

    //Calculate PID Actuation, as MechVent speed        // TODO: (steps/second)??
    _dre.setMechVentActuation_Speed(mechVentPID.calculateActuation(_positionSetpoint, _dre.getMechVentActuation_Position()));

}

void MotorControl::exitExs(u32 now_millis)
{
    _dre.setCyclePeepPressure(tempPeepPressure);
    _dre.setCycleDrivingPressure(_dre.getCyclePlateauPressure() - tempPeepPressure);
    u16 cycleDuration = now_millis - _dre.getCycleStartTime(); //TODO: check behaviour on each overflow case
    _dre.setCycleDuration(cycleDuration);
    _dre.setCycleRpmLast30s(calculateAvgRpm(cycleDuration));
}

/**
 * @brief Calculate Average RPM, over the last 30 seconds (and a little more).
 * @param cycleDuration of the just finished cycle, to be added to the Average RPM Buffer.
 * Internally, cycleDuration is reduced to 8 bits: (u8)(cycleDuration >> 7)
 */
u16 MotorControl::calculateAvgRpm(u8 cycleDuration)
{
    u16 accumulatedTime = 0;
    u16 respCycleCounter = 0;
    u8 respCycleDuration[1];
//    respCycleDuration[0] = 0;

    getAvgRpmBuffer().pushByte((u8)(cycleDuration >> 7));

    while ((accumulatedTime < 30000) || ((getAvgRpmBuffer().getBytesCount()) > respCycleCounter))
    {        
        getAvgRpmBuffer().peekByIndex(*respCycleDuration, getAvgRpmBuffer().getBytesCount() - (1 + respCycleCounter));
        accumulatedTime += respCycleDuration[0];
        respCycleCounter++;
    }
    
    return (accumulatedTime / respCycleCounter) << 7;
}

/**
 * @brief Calculate Simulated Sensors.
 */
void MotorControl::calculateSimulatedSensors(bool insufflation) // true for insufflation, false for exsufflation.
{
    // array...

    //TODO: evolve to good enough transfer function and add random pressure noise.

    if (insufflation)
    {
        _dre.setPressureMeasure(_dre.getMechVentActuation_Speed());
        _dre.setFlowMeasure(_dre.getFlowMeasure() + (_dre.getMechVentActuation_Speed() / _dre.getTimeStep()));
    }
    else
    {
        _dre.setPressureMeasure(_dre.getPeepValveActuation());
        _dre.setFlowMeasure(_dre.getFlowMeasure() + (_dre.getPeepValveActuation() / _dre.getTimeStep()));
    }
}

s16 MotorControl::getPressureSetpoint()
{
    return _pressureSetpoint;
}

s16 MotorControl::getFlowSetpoint()
{
    return _flowSetpoint;
}

}; // namespace reespirator

