/** MotorControl.hpp
 *
 * MotorControl class.
 *
 * @version 1.0.0
 * @author David Martín <david.martin.zaragoza@gmail.com>
 * @license GNU General Public License v3.0
 *
 */

#ifndef _MOTOR_CONTROL_H
#define _MOTOR_CONTROL_H

#include <float.h> 
#include <inttypes.h>
#include <LoopTicker.hpp>
#include <reespirator/Core/DRE.hpp>

//#include "pinout.h"
//#include "defaults.h"
//#include "calc.h"
//#include "Sensors.h"
//#include <AutoPID.h>
//#include <FlexyStepper.h>

namespace reespirator {

/**  TODO: Rewrite
 * @brief Human-Machine Interface manager for Reespirator serial monitor protocol.
 * @see Protocol specification: https://bit.ly/reespirator-monitor-protocol
 * 
 * It does just one subtask for each ticker update call. Subtasks are:
 * - Receive and process incoming frame from remote host (it includes sending 
 *   some response related to the incoming frame, when needed).
 * - Detect changes in DRI container to send alarm events and new cycle data 
 *   to remote host.
 * 
 * Outgoing frames are composed in this manager but copied to the SerialPort 
 * buffer. No window buffers here but generated frames can be composed
 * again with fresh data in case of remote host doesn't ACK frames.
 *  
 */
class MotorControl
{
    public:

        /**
         * @brief Construct a new Motor Control object.
         * 
         * @param dre Reference to the Reespirator DRE container.
         */
        MotorControl(DRE& dre);

        /**
         * @brief Task entry-point to motor control cycle: enter insufflation, Pressure Control mode.
         * 
         * @param now_millis 
         */
        void enterInsPC(u32 now_millis);

        /**
         * @brief Task entry-point to motor control cycle: update insufflation, Pressure Control mode
         * 
         * @param now_millis 
         */
        void updateInsPC(u32 now_millis);

        /**
         * @brief Task entry-point to motor control cycle: exit insufflation, Pressure Control mode
         * 
         * @param now_millis 
         */
        void exitInsPC(u32 now_millis);

        /**
         * @brief Task entry-point to motor control cycle: enter insufflation, Volume Control mode.
         * 
         * @param now_millis 
         */
        void enterInsVC(u32 now_millis);

        /**
         * @brief Task entry-point to motor control cycle: update insufflation, Volume Control mode
         * 
         * @param now_millis 
         */
        void updateInsVC(u32 now_millis);

        /**
         * @brief Task entry-point to motor control cycle: exit insufflation, Volume Control mode
         * 
         * @param now_millis 
         */
        void exitInsVC(u32 now_millis);

         /**
         * @brief Task entry-point to motor control cycle: enter insufflation, Volume Control Regulated by Pressure mode.
         * 
         * @param now_millis 
         */
        void enterInsVCRP(u32 now_millis);

        /**
         * @brief Task entry-point to motor control cycle: update insufflation, Volume Control Regulated by Pressure mode
         * 
         * @param now_millis 
         */
        void updateInsVCRP(u32 now_millis);

        /**
         * @brief Task entry-point to motor control cycle: exit insufflation, Volume Control Regulated by Pressure mode
         * 
         * @param now_millis 
         */
        void exitInsVCRP(u32 now_millis);

        /**
         * @brief Task entry-point to motor control cycle: enter insufflation, Pressure Control mode.
         * 
         * @param now_millis 
         */
        void enterInsRecruit(u32 now_millis);

        /**
         * @brief Task entry-point to motor control cycle: update insufflation, Pressure Control mode
         * 
         * @param now_millis 
         */
        void updateInsRecruit(u32 now_millis);

        /**
         * @brief Task entry-point to motor control cycle: exit insufflation, Pressure Control mode
         * 
         * @param now_millis 
         */
        void exitInsRecruit(u32 now_millis);

         /**
         * @brief Task entry-point to motor control cycle: enter exsufflation, all modes
         * 
         * @param now_millis 
         */
        void enterExs(u32 now_millis);

        /**
         * @brief Task entry-point to motor control cycle: update exsufflation, all modes
         * 
         * @param now_millis 
         */
        void updateExs(u32 now_millis);

        /**
         * @brief Task entry-point to motor control cycle: exit exsufflation, all modes
         * 
         * @param now_millis 
         */
        void exitExs(u32 now_millis);

        /**
         * @brief Calculate Average RPM, over the last 30 seconds (and a little more).
         * @param cycleDuration of the just finished cycle, to be added to the Average RPM Buffer.
         * Internally, cycleDuration is reduced to 8 bits: (u8)(cycleDuration >> 7)
         */
        u16 calculateAvgRpm(u8 cycleDuration);

        /**
         * @brief Calculate Simulated Sensors.
         * @param insufflation: true for insufflation, false for exsufflation.
         */
        void calculateSimulatedSensors(bool insufflation);

        //FOR TEST
        
        DRE getDRE()
        {
            return _dre;
        }

        s16 getPressureSetpoint();

        s16 getFlowSetpoint();

       // /**
        //  * @brief Task entry-point to process motor control cycle.
        //  * 
        //  * @param now_millis 
        //  */
        // void update(u32 now_millis);

        // /**
        //  * @brief Static class method to allow the deference of the object instance.
        //  * 
        //  * @param object_ptr Pointer to the MotorControl instance.
        //  * @param loop_ticker Pointer to the LoopTicker helper calling this method.
        //  */
        // static void tickUpdate(const void* object_ptr, LoopTicker* loop_ticker)
        // {
        //     ((MotorControl*) object_ptr)->update(loop_ticker->getLoopMs32());
        // }

    protected:

        CircularBuffer& getAvgRpmBuffer()
        {
            return _dre.getMcAvgRpmBuffer();
        }

    private:
        DRE _dre;

};

}; // namespace reespirator

#endif // _MOTOR_CONTROL_H