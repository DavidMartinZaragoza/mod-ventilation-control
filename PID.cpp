/** PID.cpp
 *
 * Data runtime environment.
 *
 * @version 1.0.0
 * @author Rafa Couto <caligari@treboada.net>
 * @author David Martín <david.martin.zaragoza@gmail.com>
 * @license GNU General Public License v3.0
 *
 */

#include "PID.hpp"

namespace reespirator 
{

/**
 * @brief Construct a new PID object.
 */
PID::PID(u8 kP, u8 kI, u8 kD, u8 maxSlopeP, u8 maxSlopeD, s16 outputMin, s16 outputMax, u8 timeStep)
{
    _kP = kP;
    _kI = kI;
    _kD = kD;
    _maxSlopeP = maxSlopeP;
    _maxSlopeD = maxSlopeD;
    _outputMin = outputMin;
    _outputMax = outputMax;
    _timeStep = timeStep;
    _previousError = 0;
    _errorIntegral = 0;
}

/**
 * @brief Allows setting all the PID parameters (except timeStep).
 * kP, kI, kD: Integers coded with 2 decimal positions.
 */
void PID::setParameters(u8 kP, u8 kI, u8 kD, u8 maxSlopeP, u8 maxSlopeD, s16 outputMin, s16 outputMax)
{
    _kP = kP;
    _kI = kI;
    _kD = kD;
    _maxSlopeP = maxSlopeP;
    _maxSlopeD = maxSlopeD;
    _outputMin = outputMin;
    _outputMax = outputMax;
    _previousError = 0;
    _errorIntegral = 0;
}

/**
 * @brief Allows setting the Proportional constant.
 */
void PID::setKP(u8 kP)
{
    _kP = kP;
}

/**
 * @brief Allows setting the Integral constant.
 */
void PID::setKI(u8 kI)
{
    _kI = kI;
}

/**
 * @brief Allows setting the Derivative constant.
 */
void PID::setKD(u8 kD)
{
    _kD = kD;
}

/**
 * @brief Allows setting the Maximum Proportional Slope.
 */
void PID::setMaxSlopeP(u8 maxSlopeP)
{
    _maxSlopeP = maxSlopeP;
}

/**
 * @brief Allows setting the Maximum Derivative Slope.
 */
void PID::setMaxSlopeD(u8 maxSlopeD)
{
    _maxSlopeD = maxSlopeD;
}

/**
 * @brief Checks if the sistem feedback is in within the given distance to the setpoint.
 */
bool PID::atSetpoint(u8 positiveThreshold, u8 negativeThreshold)
{
    return (((-_error) <= positiveThreshold) && (_error <= negativeThreshold));
}

/**
 * @brief Calculates PID and returns Actuation (Output) value.
 */
u16 PID::calculateActuation(s16 setpoint, s16 feedback)
{
    s8 _errorDerivative;
    s8 _s8temp;
    s16 _s16temp;

    // Error: calculation for s8 type & overflow management
    _s16temp = setpoint - feedback;
    if (_s16temp > 0x7F) { _error = 0x7F; } else //Positive overflow management
    if (_s16temp < -0x80) { _error = -0x80; } else //Negative overflow management
        { _error = _s16temp; }
    
    // Error: maxSlope management
    if (_error > _previousError)
    {   //_error has increased
        _s16temp = _previousError + _maxSlopeP; //maximum possible _error value
        if (_error > _s16temp) { _error = _s16temp; } //limit to _maxSlopeP positive change
    }
    else
    {   //_error has decreased
        _s16temp = _previousError - _maxSlopeP; //minimum possible _error value
        if (_error < _s16temp) { _error = _s16temp; } //limit to _maxSlopeP negative change
    }

    // Error derivative: calculation for s8 type, overflow & maxSlope management
    _s16temp = _error - _previousError;
    if (_s16temp > _maxSlopeD) { _s16temp = _maxSlopeD; } else //Positive overflow & maxSlope management
    if (_s16temp < -_maxSlopeD) { _s16temp = -_maxSlopeD; } //Negative overflow & maxSlope management

    _errorDerivative = ((s8)_s16temp / _timeStep) /* / 1000 */;
    
    // Error integral: calculation for s8 type & overflow management
    _s16temp = _error * _timeStep /* / 1000 */;
    if (_s16temp > 0x7F) { _s8temp = 0x7F; } else //Positive overflow management
    if (_s16temp < -0x80) { _s8temp = -0x80; } else //Negative overflow management
        { _s8temp = _s16temp; }

    _s16temp = _errorIntegral + _s8temp;
    if (_s16temp > 0x7F) { _errorIntegral = 0x7F; } else //Positive overflow management
    if (_s16temp < -0x80) { _errorIntegral = -0x80; } else //Negative overflow management
        { _errorIntegral = _s16temp; }

    // Before return:
    _previousError = _error;

    // Actuation: calculation & overflow management
    s16 _pContrib = _kP * _error;
    s16 _iContrib = _kI * _errorIntegral;
    s16 _dContrib = _kD * _errorDerivative;
    s16 _actuation;

    _s16temp = _pContrib + _iContrib;
    if ((_s16temp < 0) && (_pContrib > 0) && (_iContrib > 0)) { _s16temp = 0x7FFF; } else //Positive overflow management
    if ((_s16temp > 0) && (_pContrib < 0) && (_iContrib < 0)) { _s16temp = -0x8000; } //Negative overflow management

    _actuation = _s16temp + _dContrib;
    if ((_actuation < 0) && (_s16temp > 0) && (_dContrib > 0)) { _actuation = 0x7FFF; } else //Positive overflow management
    if ((_actuation > 0) && (_s16temp < 0) && (_dContrib < 0)) { _actuation = -0x8000; } //Negative overflow management

    // Actuation: _outputMin/Max management
    if (_actuation > _outputMax) { _actuation = _outputMax; } else
    if (_actuation < _outputMin) { _actuation = _outputMin; }

    return _actuation;
}

/**
 * @brief Resets the PID.
 */
void PID::reset()
{
    _previousError = 0;
    _errorIntegral = 0;
}

}; // namespace reespirator 